'use strict'

const button = document.getElementById('dice-button')
const adviceHeading = document.querySelector('.advice-heading')
const adviceText= document.querySelector('.advice-body')

updateAdvice()

button.addEventListener('click', updateAdvice)

function updateAdvice() {
  fetch('https://api.adviceslip.com/advice')
    .then(res => res.json())
    .then(({ slip }) => {
      const { id, advice } = slip
      
      adviceHeading.innerText = `Advice #${id}`
      adviceText.innerText = `“${advice}”`
    })
}
